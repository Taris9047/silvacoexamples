# Description
This repository contains some example input decks for Silvaco device simulator tutorial.

# Prerequisites
1. Access to Silvaco simulator (obviously...)
2. Some text editor.
3. Remote desktop environment or ...
4. Silvaco TCAD running on own machine
5. Git!

